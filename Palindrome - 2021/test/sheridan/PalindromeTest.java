package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isPalindrome = Palindrome.isPalindrome("racecar");
		assertTrue("Unable to validate Palindrome", isPalindrome);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testIsPalindromeException( ) {
		boolean isPalindrome = Palindrome.isPalindrome("");
		fail("Expected exception was not found");
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("r");
		assertTrue("Unable to validate Palindrome", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("raceca");
		assertFalse("Unable to validate Palindrome", isPalindrome);
	}	
	
}
